%%% @doc
%%% Vanillae Simple HTTP Logging Daemon Client Service Supervisor
%%%
%%% This is the service-level supervisor of the system. It is the parent of both the
%%% client connection handlers and the client manager (which manages the client
%%% connection handlers). This is the child of vl_sup.
%%%
%%% See: http://erlang.org/doc/apps/kernel/application.html
%%% @end

-module(vl_clients).
-vsn("0.1.0").
-behavior(supervisor).
-author("Peter Harpending").
-copyright("Peter Harpending").
-license("MIT").

-export([start_link/0]).
-export([init/1]).


-spec start_link() -> {ok, pid()}.
%% @private
%% This supervisor's own start function.

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, none).

-spec init(none) -> {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.
%% @private
%% The OTP init/1 function.

init(none) ->
    RestartStrategy = {rest_for_one, 1, 60},
    ClientMan = {vl_client_man,
                 {vl_client_man, start_link, []},
                 permanent,
                 5000,
                 worker,
                 [vl_client_man]},
    ClientSup = {vl_client_sup,
                 {vl_client_sup, start_link, []},
                 permanent,
                 5000,
                 supervisor,
                 [vl_client_sup]},
    Children  = [ClientSup, ClientMan],
    {ok, {RestartStrategy, Children}}.
