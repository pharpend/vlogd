# Vanillae HTTP Logging Daemon

This is a simple HTTP server that just prints out the request it received to
the console, and returns back a `204 No Content` response back to the client

I wrote this for another project, which logs stuff to an HTTP endpoint. And I
wanted an HTTP server that just prints out what it got sent. There was nothing
out there at the correct level of unsophistication. Netcat was almost perfect,
but netcat doesn't send back responses, so my logging code would indicate an
HTTP error.

This is not a real HTTP server. It doesn't even parse the request. Please do
not use this for anything important.

### Example

Client perspective:

```
[~] % curl -v localhost:8841
* Rebuilt URL to: localhost:8841/
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8841 (#0)
> GET / HTTP/1.1
> Host: localhost:8841
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 204 No Content
< Server: vlogd/0.1.0
< Connection: keep-alive
< Date: Sat, 30 Jul 2022 04:14:09 GMT
< 
* Connection #0 to host localhost left intact
```

Server perspective:

```
[vanillae/vlogd master] % zxh runlocal
Erlang/OTP 23 [erts-11.1.1] [source] [64-bit] [smp:24:24] [ds:24:24:10] [async-threads:1] [hipe]

Eshell V11.1.1  (abort with ^G)
1> Starting otpr-vlogd-0.1.0.
Starting.
<0.143.0> Listening.
Started [vlogd]
<0.144.0> Listening.
<0.143.0> Connection accepted from: {{0,0,0,0,0,65535,32512,1},38506}
Monitoring <0.143.0> @ #Ref<0.385224486.2976645121.134177>
<0.143.0> < GET / HTTP/1.1
<0.143.0> < Host: localhost:8841
<0.143.0> < User-Agent: curl/7.58.0
<0.143.0> < Accept: */*
<0.143.0> < 
<0.143.0> < 
<0.143.0> > HTTP/1.1 204 No Content
<0.143.0> > Server: vlogd/0.1.0
<0.143.0> > Connection: keep-alive
<0.143.0> > Date: Sat, 30 Jul 2022 04:14:09 GMT
<0.143.0> > 
<0.143.0> > 
<0.143.0> Socket closed, retiring.
```

# How to use

1. [Install Erlang and zx](https://www.bitchute.com/video/1gCvcoPUR7eJ/)
2. `git clone https://gitlab.com/pharpend/vlogd.git`
3. `cd vlogd`
4. `zxh runlocal`

By default, it listens to port `8841`. If you want to change this, you can
either

1.  call `vlogd:ignore()` and `vlogd:listen(NewPortNum)` in the zxh shell:

    ```
    2> vlogd:ignore(), vlogd:listen(8000).    
    <0.144.0> Retiring: Listen socket closed.
    <0.147.0> Listening.
    ok
    ```

2.  edit this function in `src/vlogd.erl`:

    ```erlang
    start(normal, _Args) ->
        Result = vl_sup:start_link(),
        ok = listen(8841),
        Result.
    ```
